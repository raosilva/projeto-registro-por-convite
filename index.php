<?php

session_start();

require 'config.php';

if(empty($_SESSION['loggedId'])){
    header("Location:login.php");
    exit;
}

$sql = "SELECT * FROM users WHERE id = :id";
$sql = $pdo->prepare($sql);
$sql->bindValue(":id", $_SESSION['loggedId']);
$sql->execute();

if($sql->rowCount() > 0 ){
    $user = $sql->fetch();
}

$dirAddress = str_replace("/var/www/html/projeto-registroporconvite","http://projeto-registroporconvite.test",dirname(__FILE__));

?>

<h1>Área Restrita</h1><br>
<hr><br>
<strong>Usuário:</strong> <?=$user['email']; ?> (<?=$user['nome']; ?>) || <a href="logout.php">Logout</a><br><br>

<p><strong>Convide um amigo:</strong> <?=$dirAddress; ?>/signup.php?codigo=<?=$user['codigo']; ?></p>