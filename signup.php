<?php

session_start();

require 'config.php';

$codigo = filter_input(INPUT_GET, 'codigo', FILTER_SANITIZE_SPECIAL_CHARS);
$nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$passwd = md5(filter_input(INPUT_POST, "passwd"));
$user;
if(!empty($codigo)){
    $sql = ("SELECT * FROM users WHERE codigo = :codigo AND cod_count < 5");
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":codigo", $codigo);
    $sql->execute();   
    if($sql->rowCount() == 5 ){
        header("Location: logout.php");
        exit;
    }
    $user = $sql->fetch();
} else {
    header("Location: logout.php");
    exit;
}
if(isset($email) && !empty($email)){
    $sql = "SELECT * FROM users WHERE email = :email";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":email", $email);
    $sql->execute();

    if($sql->rowCount() <= 0){
        $userCod = md5(rand(0,99999).rand(0,99999));
        $sql = "INSERT INTO users (nome, email, passwd, codigo, cod_count) VALUES (:nome, :email, :passwd, :codigo, :cod_count)";
        $sql = $pdo->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":passwd", $passwd);
        $sql->bindValue(":codigo", $userCod);
        $sql->bindValue(":cod_count", "0");
        $sql->execute();
        
        $codAvail = $user['cod_count'];         
        $sql = "UPDATE users SET cod_count = :cod_count WHERE codigo = :codigo";
        $sql = $pdo->prepare($sql);
        $sql->bindValue(":cod_count", ++$codAvail );
        $sql->bindValue(":codigo", $codigo );
        $sql->execute();
        header('Location:logout.php');
    }
} 
?>
<fieldset style="width:50vw; margin: 0 auto;">
    <legend>Cadastro de Novo Usuário</legend>
    <form method="POST">
        Nome: <input type="text" name="nome"><br><br>
        E-mail: <input type="email" name="email"><br><br>
        Senha: <input type="password" name="passwd"><br><br>
        <input type="submit" value="Cadastrar" />
    </form>
</fieldset>