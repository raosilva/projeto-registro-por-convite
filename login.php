<?php

session_start();
session_regenerate_id(true); 

require 'config.php';

$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$passwd = md5(filter_input(INPUT_POST, 'passwd'));
$_SESSION['loggedId'] = "";

if((isset($email) && !empty($email))){
    $sql = "SELECT * FROM users WHERE email = :email AND passwd = :passwd";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":email", $email);
    $sql->bindValue(":passwd", $passwd);
    $sql->execute();  

    if($sql->rowCount() > 0){
        $user = $sql->fetch();
        $_SESSION['loggedId'] = $user['id'];
        header("Location:index.php");
    }
}
?>

<fieldset style="width:50vw; margin: 0 auto; border:0">
    <form method="POST">
        E-mail: <input type="email" name="email"><br><br>
        Senha: <input type="password" name="passwd"><br><br>
        <input type="submit" value="Entrar" />
        <a href="signup.php">Cadastrar</a>
    </form>
</fieldset>